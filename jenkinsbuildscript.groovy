def FAILED_STAGE

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                cleanWs()
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/lizalisa/meow']]])
                sh "git status"
                sh "source scripts/initial_submodule.sh"
                sh """
                wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
                bash miniconda.sh -b -p $WORKSPACE/miniconda
                source $WORKSPACE/miniconda/etc/profile.d/conda.sh
                conda create --name tg_env
                conda activate tg_env
                conda install python==3.10.4
                conda install requests
                """
            }
        }
        // stage('Prepare working directory') {
        //     steps {
        //         checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/lizalisa/meow']]])
        //         sh "git status"
        //     }
        // }
        stage('Run script1') {
            steps {
                script {
                    FAILED_STAGE=env.STAGE_NAME
                }
                sh """
                source $WORKSPACE/miniconda/etc/profile.d/conda.sh
                conda activate tg_env
                cd gradle
                ./gradlew run_script1 -i
                """
            }
        }
        stage('Run script2') {
            steps {
                script {
                    FAILED_STAGE=env.STAGE_NAME
                }
                sh """
                source $WORKSPACE/miniconda/etc/profile.d/conda.sh
                conda activate tg_env
                cd gradle
                ./gradlew run_script2 -i
                """
            }
        }
        stage('Run test file') {
            steps {
                script {
                    FAILED_STAGE=env.STAGE_NAME
                }
                sh """
                source $WORKSPACE/miniconda/etc/profile.d/conda.sh
                conda activate tg_env
                cd gradle
                ./gradlew testing_tmp -i
                """
            }
        }
    }

    post {
        success { 
            sh """
            source $WORKSPACE/miniconda/etc/profile.d/conda.sh
            conda activate tg_env
            cd tg
            python -m notify --message="World is done! :)" --token_id="5721854044:AAEYyktdJIbY7rKTE3AaOx6B2B-FbUaZ8YI" --chat_id="-1001727415822"
            """
        }

        failure{
            sh """
            source $WORKSPACE/miniconda/etc/profile.d/conda.sh
            conda activate tg_env
            cd tg
            python -m notify --message="Fail in ${FAILED_STAGE}" --token_id="5721854044:AAEYyktdJIbY7rKTE3AaOx6B2B-FbUaZ8YI" --chat_id="-1001727415822"
            """
        }
    }
}
